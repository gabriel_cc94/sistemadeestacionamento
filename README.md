# Sistema Estacionamento

 
## Tópicos

* [Instalação](#instalacao)
    * [Virtualenv](#virtualenv)
    * [Docker e docker-compose](#docker-e-docker-compose)
* [Como usar](#como-usar)
    * [Desenvolvimento](#desenvolvimento)
    * [Testes](#testes)
* [Debug](#debug)

 ## Instalação
Siga os passos abaixo para executar a aplicação

 ### Virtualenv
 
 ***Windows***
 - https://www.treinaweb.com.br/blog/criando-ambientes-virtuais-para-projetos-python-com-o-virtualenv

 ***Linux***
 - http://devfuria.com.br/linux/instalando-virtualenv/

 ### Docker e Docker-compose
Consulte as documentações oficiais para realizar as instalações

Docker: `https://docs.docker.com/engine/install/`

Docker-compose: `https://docs.docker.com/compose/install/`

Caso deseje executar o projeto sem docker(não é recomendado) consulte o `Dockerfile` localizado na pasta
`build/` para consultar as dependências e o processo de build e execução do projeto.


 ## Passo a passo

***Windows***

Baixar e instalar o Docker Desktop (`https://docs.docker.com/desktop/windows/install/`).

Abrir os Recursos do Windows.

`1.Ativar o Subsistema do Linux pro Windows.`
`2.Ativar a plataforma de máquina virtual.`
`3.(se aparecer a opção) Ativar o Hyper-V.`

(Se estiver desativado) Ativar a virtualização pela BIOS/UEFI do PC || Segue um tutorial aqui: (`https://www.youtube.com/watch?v=ZDeje9wgDp4`).

Abrir o prompt ou Powershell (ADMIN) e dar o seguinte comando: `wsl --install`

Rodar o WSL (ADMIN) no Windows.

Abrir o Docker Desktop.

 [Voltar ao topo](#tópicos)
 
 ## Como usar

 ### Desenvolvimento

  #### Via env
 ```bash
  source NOME_ENV/bin/activate
  python manage.py runserver
```

 #### Via container
 ```bash
docker-compose -f deployments/docker-compose.desenvolvimento.yml --compatibility up --build
```

 #### Rodar atualizacoes dos models no banco de dados
 ```bash
  python manage.py makemigrations
  python manage.py migrate
```

 #### Criar superusuario
 ```bash
  python manage.py createsuperuser
```


[Voltar ao topo](#tópicos)

 ## Testes

- Execucao
```bash
python manage.py test apps.estacionamento.tests  --timing --traceback --force-color
```

[Voltar ao topo](#tópicos)
